*** 0.1a

+ Initial build. Faulty controls. Basic minecraft mechanics.


*** 0.2.1a

+ Map Improvements
	++ Bigger map
	++ Meshes for better performance

+ Fixed controls.
+ Added sound effects
+ Esc button will close the game


*** 0.2.2a

+ Map Improvements
	++ Perlin Noise map generation (test)
	++ Improved performance(fps) but slower loading time


*** 0.3.1a

+ Map now loads faster. Added an additional loading screen
+ Removed Perlin Noise generation
+ Added new block: deep dirt block
+ Added an application icon
+ Improved block clicking


*** 0.3.2a

+ Removed ability to spawn blocks
+ Added tools: Hand & Shovel
+ Game mechanic: "Block Support"
+ Added inventory system (incomplete)
+ Blocks now drop items
+ Map loading & generation has been improved further


*** 0.3.3a

+ Readded sky (default)
+ Changed graphics
+ Inventory system is now more functional
+ Faster map loading


*** 0.4.1a

+ Minor graphic changes
+ Added Wood and Leaves blocks
	++ Added Trees. They spawn randomly during the generation
+ Added Axe tool. Used to break Wood blocks
+ Shop System preparation
	++ Designed the shop window
	++ Added Balance(Gold)
	++ The shop is not functional
+ Fixed a map generation bug from previous version
