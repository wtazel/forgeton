﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombinedBrick : MonoBehaviour {

	void Start () {
		//CombineM.combineMeshes (this.gameObject);
	} 

	void OnMouseOver() {
		if (Input.GetMouseButtonDown (0)) {
			//if(this.gameObject.tag != "Combined")
				CombineM.splitMeshes (this.gameObject);
			StartCoroutine (Recombine ());
		}
		if (Input.GetMouseButtonDown (1)) {
			//if(this.gameObject.tag != "Combined")
			CombineM.splitMeshes (this.gameObject);
			StartCoroutine (Recombine ());
		}
	}

	IEnumerator Recombine()
	{
		yield return new WaitForSeconds(2);
		CombineM.CombineX (this.gameObject);

	}

	void OnBecameInvisible() {
		enabled = false;
	}

	void OnBecameVisible() {
		enabled = true;

	}
	

}
