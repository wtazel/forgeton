﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMap : MonoBehaviour {
	
	uint chunkAmount = 18*18; //Must be a perfect square
	uint chunkLength = 5; //Blocks per chunk side

	int i, j, k;
	bool wasGen = false;

	Vector3 pos;
	GameObject _brick;

	//List<GameObject> grassBlocks, dirtBlocks, deepDirtBlocks, chunks;
	List<GameObject> chunks;

	GameObject prefabGrassBlock, prefabDirtBlock, prefabDeepDirtBlock;

	void Start () {

		/*grassBlocks = InitChunkParent ("Grass Blocks");
		dirtBlocks = InitChunkParent ("Dirt Blocks");
		deepDirtBlocks = InitChunkParent ("DeepDirt Blocks");*/
		chunks = InitChunkParent ("Chunks");

		prefabGrassBlock = Resources.Load<GameObject> ("Prefabs/" + "Grass Block");
		prefabDirtBlock = Resources.Load<GameObject> ("Prefabs/" + "Dirt Block");
		prefabDeepDirtBlock = Resources.Load<GameObject> ("Prefabs/" + "Deep Dirt Block");

		k = 0; // Chunk number 0
		i = 0;
		j = i;
	}
		
	void Update() {
		if (!wasGen) {

			pos = new Vector3 (i, 0, j);
			_brick = InstantianteBrick (prefabGrassBlock, pos, k);
			if(k+1<chunkAmount)
				_brick = InstantianteBrick (prefabGrassBlock, pos, k+1);

			if (j != i) {
				pos = new Vector3 (j, 0, i);
				_brick = InstantianteBrick (prefabGrassBlock, pos, k);
				if(k+1<chunkAmount)
					_brick = InstantianteBrick (prefabGrassBlock, pos, k+1);
			}
			//chunkIndex = (int)i / 10 * chunkL + (int)j / 10;

			for (float u = pos.y - 1f; u > pos.y - chunkLength; u--) {
				if (u < pos.y - 7f) {
					InstantianteBrick (prefabDeepDirtBlock, new Vector3 (i, u, j), k);
					if(k+1<chunkAmount)
						InstantianteBrick (prefabDeepDirtBlock, new Vector3 (i, u, j), k+1);
					if (j != i) {
						InstantianteBrick (prefabDeepDirtBlock, new Vector3 (j, u, i), k);
						if(k+1<chunkAmount)
							InstantianteBrick (prefabDeepDirtBlock, new Vector3 (j, u, i), k + 1);
					}
					
				} else {
					InstantianteBrick (prefabDirtBlock, new Vector3 (i, u, j), k);
					if(k+1<chunkAmount)
						InstantianteBrick (prefabDirtBlock, new Vector3 (i, u, j), k+1);
					if (j != i) {
						InstantianteBrick (prefabDirtBlock, new Vector3 (j, u, i), k);
						if(k+1<chunkAmount)
							InstantianteBrick (prefabDirtBlock, new Vector3 (j, u, i), k+1);
					}

				}
			} 

			if(Random.Range(0,99) == 5) //10% chance of a tree to spawn in the current position (i,j)
				Instantiate (Resources.Load<GameObject> ("Prefabs/Tree"), new Vector3 (i, 6, j), Quaternion.identity).transform.parent = chunks[k].transform;

			j++;
			if (j == chunkLength) {
				
				i++;
				j = i;
				if (i==chunkLength) {

					i = 0;
					j = i;

					CombineM.CombineX (chunks [k]);

					/*CombineM.combineMeshes (grassBlocks[k]);
					CombineM.combineMeshes (dirtBlocks[k]);
					CombineM.combineMeshes (deepDirtBlocks[k]);*/

					if (k + 1 < chunkAmount) {
						/*
						CombineM.combineMeshes (grassBlocks [k + 1]);
						CombineM.combineMeshes (dirtBlocks [k + 1]);
						CombineM.combineMeshes (deepDirtBlocks [k + 1]);*/

						CombineM.CombineX (chunks [k+1]);
					}

					/*grassBlocks [k].transform.position = new Vector3 (16 * (k % Mathf.Sqrt(chunkAmount)), 0, 16 * Mathf.Floor (k / Mathf.Sqrt(chunkAmount)));
					dirtBlocks [k].transform.position = new Vector3 (16 * (k % Mathf.Sqrt(chunkAmount)), 0, 16 * Mathf.Floor (k / Mathf.Sqrt(chunkAmount)));
					deepDirtBlocks [k].transform.position = new Vector3 (16 *( k % Mathf.Sqrt(chunkAmount)), 0, 16 * Mathf.Floor (k / Mathf.Sqrt(chunkAmount)));*/

					chunks [k].transform.position = new Vector3 (chunkLength *( k % Mathf.Sqrt(chunkAmount)), 0, chunkLength * Mathf.Floor (k / Mathf.Sqrt(chunkAmount)));

					if (k + 1 < chunkAmount) {
						/*grassBlocks [k + 1].transform.position = new Vector3 (16 * ((k + 1) % Mathf.Sqrt (chunkAmount)), 0, 16 * Mathf.Floor ((k + 1) / Mathf.Sqrt (chunkAmount)));
						dirtBlocks [k + 1].transform.position = new Vector3 (16 * ((k + 1) % Mathf.Sqrt (chunkAmount)), 0, 16 * Mathf.Floor ((k + 1) / Mathf.Sqrt (chunkAmount)));
						deepDirtBlocks [k + 1].transform.position = new Vector3 (16 * ((k + 1) % Mathf.Sqrt (chunkAmount)), 0, 16 * Mathf.Floor ((k + 1) / Mathf.Sqrt (chunkAmount)));*/

						chunks [k + 1].transform.position = new Vector3 (chunkLength * ((k + 1) % Mathf.Sqrt (chunkAmount)), 0, chunkLength * Mathf.Floor ((k + 1) / Mathf.Sqrt (chunkAmount)));
					}

					/*grassBlocks [k].GetComponentInChildren<MeshRenderer> (true).enabled = true;
					if(k+1<chunkAmount)
						grassBlocks [k+1].GetComponentInChildren<MeshRenderer> (true).enabled = true;
*/
					k+=2;
					if (k >= chunkAmount) {
						
						wasGen = true;
					}
				}

			}
		}
	}


	/**
	 * Initializes a GameObject that will become parent to a chunk of blocks.
	 */

	List<GameObject> InitChunkParent( string gameobjName, uint amount = 0) {
		List<GameObject> gameobj = new List<GameObject> ();

		for (int i = 0; i < ((amount!=0)?amount:chunkAmount); i++) {
			gameobj.Add (new GameObject (gameobjName + " CK" + i.ToString ()));
			gameobj[i].AddComponent<MeshFilter> ();
			gameobj[i].AddComponent<MeshRenderer> ();
			gameobj[i].AddComponent<CombinedBrick> ();
		}

		return gameobj;
	}

	GameObject InstantianteBrick(GameObject prefab, Vector3 pos, int chunk) {
		GameObject g = GameObject.Instantiate (prefab, pos, Quaternion.identity);
		/*if (prefab.name == "Grass Block") {
			g.GetComponent<Brick> ().blockTypeId = BlockDefines.block_grass;
			g.transform.parent = grassBlocks [chunk].transform;
			g.GetComponent<MeshRenderer> ().enabled = false;
		} else if (prefab.name == "Dirt Block") {
			g.GetComponent<Brick> ().blockTypeId = BlockDefines.block_dirt;
			g.transform.parent = dirtBlocks [chunk].transform;			
			g.GetComponent<MeshRenderer> ().enabled = false;
		} else if (prefab.name == "Deep Dirt Block") {
			g.GetComponent<Brick> ().blockTypeId = BlockDefines.block_deepdirt;
			g.transform.parent = deepDirtBlocks [chunk].transform;			
			g.GetComponent<MeshRenderer> ().enabled = false;
		} else if (prefab.name == "Wood Block") {
			g.GetComponent<Brick> ().blockTypeId = BlockDefines.block_wood;
			//g.transform.parent = deepDirtBlocks [chunk].transform;			
			g.GetComponent<MeshRenderer> ().enabled = false;
		} */

		g.GetComponent<Brick> ().blockTypeId = BlockDefines.block_deepdirt;
		g.transform.parent = chunks [chunk].transform;			
		g.GetComponent<MeshRenderer> ().enabled = false;

		return g;

	}

}

/*
 * ************************************************************
 * */
//			pos = new Vector3 (i, 0, j);
//
//			for (int l = 0; l < chunkAmount; l++) {
//				_brick = InstantianteBrick (prefabGrassBlock, pos, l);
//			}
//			//_brick = InstantianteBrick (prefabGrassBlock, pos, k+1);
//
//			if (j != i) {
//				pos = new Vector3 (j, 0, i);
//				for (int l = 0; l < chunkAmount; l++) {
//					_brick = InstantianteBrick (prefabGrassBlock, pos, l);
//				}
//				//_brick = InstantianteBrick (prefabGrassBlock, pos, k+1);
//			}
//			//chunkIndex = (int)i / 10 * chunkL + (int)j / 10;
//
//			for (float u = pos.y - 1f; u > pos.y - 16f; u--) {
//				if (u < pos.y - 7f) {
//					for (int l = 0; l < chunkAmount; l++) {
//						InstantianteBrick (prefabDeepDirtBlock, new Vector3 (i, u, j), l);
//					}
//					//InstantianteBrick (prefabDeepDirtBlock, new Vector3 (i, u, j), k+1);
//					if (j != i) {
//						for (int l = 0; l < chunkAmount; l++) {
//							InstantianteBrick (prefabDeepDirtBlock, new Vector3 (j, u, i), l);
//						}
//						//InstantianteBrick (prefabDeepDirtBlock, new Vector3 (j, u, i), k + 1);
//					}
//					
//				} else {
//					for (int l = 0; l < chunkAmount; l++) {
//						InstantianteBrick (prefabDirtBlock, new Vector3 (i, u, j), l);
//					}
//					//InstantianteBrick (prefabDirtBlock, new Vector3 (i, u, j), k+1);
//					if (j != i) {
//						for (int l = 0; l < chunkAmount; l++) {
//							InstantianteBrick (prefabDirtBlock, new Vector3 (j, u, i), k);
//						}
//						//InstantianteBrick (prefabDirtBlock, new Vector3 (j, u, i), k+1);
//					}
//
//				}
//			} 
//
//			j++;
//			if (j == 16) {
//				
//				i++;
//				j = i;
//				if (i==16) {
//
//					i = 0;
//					j = i;
//					for (int l = 0; l < chunkAmount; l++) {
//						CombineM.combineMeshes (grassBlocks [l]);
//						CombineM.combineMeshes (dirtBlocks [l]);
//						CombineM.combineMeshes (deepDirtBlocks [l]);
//					}
//
//					/*CombineM.combineMeshes (grassBlocks[k+1]);
//					CombineM.combineMeshes (dirtBlocks[k+1]);
//					CombineM.combineMeshes (deepDirtBlocks[k+1]);*/
//
//for (int l = 0; l < chunkAmount; l++) {
//	grassBlocks [l].transform.position = new Vector3 (16 * (l % Mathf.Sqrt (chunkAmount)), 0, 16 * Mathf.Floor (l / Mathf.Sqrt (chunkAmount)));
//	dirtBlocks [l].transform.position = new Vector3 (16 * (l % Mathf.Sqrt (chunkAmount)), 0, 16 * Mathf.Floor (l / Mathf.Sqrt (chunkAmount)));
//	deepDirtBlocks [l].transform.position = new Vector3 (16 * (l % Mathf.Sqrt (chunkAmount)), 0, 16 * Mathf.Floor (l / Mathf.Sqrt (chunkAmount)));
//}
//
///*grassBlocks [k+1].transform.position = new Vector3 (16 * ((k+1) % Mathf.Sqrt(chunkAmount)), 0, 16 * Mathf.Floor ((k+1) / Mathf.Sqrt(chunkAmount)));
//					dirtBlocks [k+1].transform.position = new Vector3 (16 * ((k+1) % Mathf.Sqrt(chunkAmount)), 0, 16 * Mathf.Floor ((k+1) / Mathf.Sqrt(chunkAmount)));
//					deepDirtBlocks [k+1].transform.position = new Vector3 (16 *( (k+1) % Mathf.Sqrt(chunkAmount)), 0, 16 * Mathf.Floor ((k+1) / Mathf.Sqrt(chunkAmount)));
//*/
//
//for (int l = 0; l < chunkAmount; l++) {
//	grassBlocks [l].GetComponentInChildren<MeshRenderer> (true).enabled = true;
//}
////grassBlocks [k+1].GetComponentInChildren<MeshRenderer> (true).enabled = true;
//
///*k+=2;
//					if (k == chunkAmount) {
//						
//						wasGen = true;
//					}*/
//
//wasGen = true;
//}
//
//}