﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseTarget : MonoBehaviour {
	
	RaycastHit hit, prevHit;

	Ray prevRay;

	bool waitForRecombine;
	int currentBlock, currentTool;
	GameObject blockParentContainer;

	GameObject mapCtrl, currentToolUI;

	void Start() {
		
		waitForRecombine = false;

		mapCtrl = GameObject.FindGameObjectWithTag ("MapCtrl");
		currentToolUI = GameObject.FindGameObjectWithTag ("CurrentToolUI");

		//currentBlock = 1;
		ChangeTool(1);
	}

	void Update() {

		/*if (Input.GetMouseButtonDown (0)) {
			CastRay ();
			if (hit.collider) {
				if (hit.transform.gameObject.tag == "Combined") {
					blockParentContainer = hit.transform.gameObject;
					CombineM.splitMeshes (hit.transform.gameObject);
					//print (hit.transform.gameObject.name);
					CastRay ();
					waitForRecombine = true;
				}

				if(hit.collider) {
					if (hit.transform.tag == "Brick") {
						GameObject.Destroy (hit.transform.gameObject);
					}

				}  

				if(waitForRecombine) {
					CombineM.combineMeshes (blockParentContainer);
					waitForRecombine = false;
				}


			}
		} else */
		/*
		if (Input.GetMouseButtonUp (1)) {
			CastRay ();
			if (hit.collider) {
				
				int chunkIndex = (int)Mathf.Floor (hit.transform.position.x / 10) * mapCtrl.GetComponent<GenerateMap> ().ChunkL + (int)Mathf.Floor (hit.transform.position.z / 10);
				if(currentBlock == 1) 
					blockParentContainer = GameObject.Find("Grass Blocks C" + chunkIndex.ToString());
				else if(currentBlock == 2) 
					blockParentContainer = GameObject.Find("Dirt Blocks C" + chunkIndex.ToString());	

				print (hit.collider);
				if(hit.transform.gameObject.tag == "Brick" && hit.collider) {
					Vector3 pos = hit.transform.position + hit.normal;
					if (currentBlock == 1) {
						Instantiate (Resources.Load<GameObject> ("Prefabs/Grass Block"), pos, Quaternion.identity).transform.parent = blockParentContainer.transform;
					} else if (currentBlock == 2) {
						Instantiate (Resources.Load<GameObject> ("Prefabs/Dirt Block"), pos, Quaternion.identity).transform.parent = blockParentContainer.transform;
					}


				}

			}
		}*/

		if (Input.GetKeyDown (KeyCode.Alpha1))
			ChangeTool (1);
		else if (Input.GetKeyDown (KeyCode.Alpha2))
			ChangeTool (2);
		else if (Input.GetKeyDown (KeyCode.Alpha3))
			ChangeTool (3);

	}
	
	void CastRay(bool castWithPreviousRay = false) {
		
		//Ray ray = transform.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f,0.5f,0.5f));
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (!castWithPreviousRay) {
			
			Physics.Raycast (ray, out hit, 5f);
			//print (hit.collider.name);

		} 
	}

	void ChangeTool(int tool) {
		currentTool = tool;
		if (tool == 1) {
			//currentToolUI.GetComponent<Text> ().text = "Hand";
			currentToolUI.transform.GetChild (0).GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Textures/hand");
		} else if (tool == 2) {
			//currentToolUI.GetComponent<Text> ().text = "Shovel";
			currentToolUI.transform.GetChild (0).GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Textures/shovel");
		}  else if (tool == 3) {
			//currentToolUI.GetComponent<Text> ().text = "Shovel";
			currentToolUI.transform.GetChild (0).GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Textures/axe");;
		}
	}

	public int CurrentTool {
		get { return this.currentTool; }
	}
		
}
