﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockDefines{

	public const int block_grass = 1;
	public const int block_dirt = 2;
	public const int block_deepdirt = 3;
	public const int block_wood = 4;
	public const int block_leaves = 5;

}
