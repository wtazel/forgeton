﻿/* This script should be used for character movement, but for now a premade script will be used. 
 * Note that this script doesn't move the character correctly.
 */

//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using System;
//
//public class CharMove : MonoBehaviour {
//
//	[SerializeField] private float speed;
//	[SerializeField] private float jumpSpeed;
//	private MouseLook mouseLook;
//	private Rigidbody rb;
//
//	void Start () {
//		mouseLook = GetComponent<MouseLook> ();
//		//rb = GetComponent<Rigidbody> ();
//	}
//	
//
//	void Update () {
//		mouseLook.LookRotation ();
//
//		//print (Math.Round (rb.velocity.y, 2));
//	}
//
//	void FixedUpdate() {
//
//		if(!IsGrounded())
//			transform.Translate (-Vector3.up * Time.deltaTime * 2.5f);
//
//		if (Input.GetKey (KeyCode.W)) {
//			//rb.MovePosition (transform.position + transform.forward * Time.deltaTime);
//			transform.Translate (transform.GetChild(0).transform.forward +  transform.forward* Time.deltaTime  * speed);
//		} else if (Input.GetKey (KeyCode.S)) {
//			//rb.MovePosition (transform.position - transform.forward * Time.deltaTime * speed);
//			transform.Translate (-transform.forward * Time.deltaTime  * speed);
//		}
//
//		if (Input.GetKey (KeyCode.A)) {
//			//rb.MovePosition (transform.position - transform.right * Time.deltaTime * speed);
//			transform.Translate (-transform.right * Time.deltaTime  * speed);
//		} else if (Input.GetKey (KeyCode.D)) {
//			//rb.MovePosition (transform.position + transform.right * Time.deltaTime * speed);
//			transform.Translate (transform.right * Time.deltaTime  * speed);
//		}
//
//		if (Input.GetKeyDown (KeyCode.Space)) {
//			if (this.IsGrounded () ) {
//				//rb.AddForce (Vector3.up * jumpSpeed*20, ForceMode.Acceleration);
//
//			}
//		}
//
//		mouseLook.UpdateCursorLock ();
//	}
//
//	private bool IsGrounded() {
//		
//		return Physics.Raycast (transform.position, -Vector3.up, 0.5f);
//	}
//
//	void OnCollisionEnter(Collision collision)
//	{
//		if(collision.contacts.Length > 0)
//		{
//			ContactPoint contact = collision.contacts[0];
//			if(Vector3.Dot(contact.normal, Vector3.up) > 0.5)
//			{
//				
//				print ("ground");
//			}
//		}
//	}
//}
