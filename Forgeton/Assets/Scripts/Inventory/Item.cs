﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item{

	private int itemId;
	private ItemName name;
	private string itemTitle;
	private string itemDesc;
	private Sprite itemImage;
	private float itemPrice;

	public enum ItemName { None = 0, Dirt = 1, GrassSeeds = 2, Wood = 3, Shovel = 4, Axe = 5, Custom = -1 };


	public Item() {
		this.name = ItemName.None;
		this.itemTitle = null;
		this.itemDesc = null;
		this.itemImage = null;
		this.itemPrice = 0.00f;
	}

	/*public Item(int id) {
		this.itemId = id;
		Autocomplete (id);
	}*/

	public Item(ItemName itemName) : this() {
		Autocomplete (itemName);
	}

	public Item(string itemTitle, string itemDesc, Sprite itemImage, float itemPrice) {
		this.name = ItemName.Custom;
		this.itemTitle = itemTitle;
		this.itemDesc = itemDesc;
		this.itemImage = itemImage;
		this.itemPrice = itemPrice;
	}

	public string ItemTitle {
		get { return this.itemTitle; }
		set { this.itemTitle = value; }
	}

	public string ItemDesc {
		get { return this.itemDesc; }
		set { this.itemDesc = value; }
	}

	public Sprite ItemImage {
		get { return this.itemImage; }
		set { this.itemImage = value; }
	}

	public float ItemPrice {
		get { return this.itemPrice; }
		set { this.itemPrice = value; }
	}

	public int ItemId {
		get { return this.itemId; }
		set { this.itemId = value; }
	}

	public ItemName Name {
		get { return this.name; }
	}

	/*private void Autocomplete(int id) {
		switch (id) {
		case 1:
			this.itemTitle = "Dirt";
			this.itemDesc = "Just some useless dirt.";
			this.itemImage = Resources.Load<Sprite> ("Textures/dirt item");
			break;
		case 2:
			this.itemTitle = "Grass Seeds";
			this.itemDesc = "Used to grow grass.";
			this.itemImage = Resources.Load<Sprite> ("Textures/grass seeds");
			break;
		case 3:
			this.itemTitle = "Wood";
			this.ItemDesc = "Nothing more than wood.";
			this.ItemImage = Resources.Load<Sprite> ("Textures/wood");
			break;
		}
	}*/

	private void Autocomplete(ItemName name) {
		switch (name) {
		case ItemName.Dirt:
			this.itemTitle = "Dirt";
			this.itemDesc = "Just some useless dirt.";
			this.itemImage = Resources.Load<Sprite> ("Textures/dirt item");
			this.itemPrice = 0.05f;
			break;
		case ItemName.GrassSeeds:
			this.itemTitle = "Grass Seeds";
			this.itemDesc = "Used to grow grass. Easy to obtain.";
			this.itemImage = Resources.Load<Sprite> ("Textures/grass seeds");
			this.itemPrice = 0.15f;
			break;
		case ItemName.Wood:
			this.itemTitle = "Wood";
			this.ItemDesc = "Nothing more than wood.";
			this.ItemImage = Resources.Load<Sprite> ("Textures/wood");
			this.itemPrice = 2.00f;
			break;
		case ItemName.Shovel:
			this.itemTitle = "Shovel";
			this.ItemDesc = "Good tool for digging";
			this.ItemImage = Resources.Load<Sprite> ("Textures/shovel");
			this.itemPrice = 25.00f;
			break;
		case ItemName.Axe:
			this.itemTitle = "Axe";
			this.ItemDesc = "Required in order to break wood";
			this.ItemImage = Resources.Load<Sprite> ("Textures/axe");
			this.itemPrice = 41.00f;
			break;
		}

		this.name = name;
	}



}
