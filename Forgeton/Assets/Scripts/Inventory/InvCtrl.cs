﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InvCtrl : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {

	Item item;
	int quantity;
	public int slotId = 0;

	GameObject invTooltip;
	CharInventory charInventory;

	void Start () {
		invTooltip = GameObject.FindGameObjectWithTag ("InvItemTooltip");	
		charInventory = GameObject.FindGameObjectWithTag ("Player").GetComponent<CharInventory>();

		UpdateSlot ();

	}


	public void OnPointerDown( PointerEventData eventData )
    {
    }
 
    public void OnPointerUp( PointerEventData eventData )
    {
    }
 
    public void OnPointerClick( PointerEventData eventData )
    {
		Debug.Log( "Clicked! " + gameObject.name );
    }

	public void OnPointerEnter(PointerEventData eventData) {
		/*invTooltip.transform.GetChild(1).GetComponent<Text> ().text = item.ItemTitle;
		invTooltip.transform.GetChild(1).GetComponent<Text> ().color = new Color(0,0,0,1);*/

	}

	public void OnPointerExit(PointerEventData eventData) {
		/*invTooltip.GetComponent<Text> ().text = "";
		invTooltip.GetComponent<Text> ().color = new Color(0,0,0,0);*/
	}

	public void UpdateSlot() {
		item = charInventory.ItemList [slotId];
		quantity = charInventory.Quantity [slotId];

		transform.GetChild(0).GetComponent<Image> ().sprite = item.ItemImage;
		transform.GetChild(0).GetComponent<Image> ().color = item.ItemImage!=null?new Color(1,1,1,1):new Color(0,0,0,0);
		transform.GetChild(1).GetComponent<Text> ().text = quantity==0?"":quantity.ToString();

	}

}
