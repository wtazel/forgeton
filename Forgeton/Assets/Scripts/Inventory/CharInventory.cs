﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;
using UnityEngine.UI;

public class CharInventory : MonoBehaviour {

	public int slotsCount = 32;
	public float charBalance = 45;

	List<GameObject> itemSlots, shopItemSlots;
	List<Item> shopItemList;
	List<Item> itemList;
	List<int> quantity;

	public enum uiElem { Inventory, Shop, All };

	GameObject inventoryUI, shopUI;
	GameObject playerCtrl; //The object where player movement controls are located.

	Text balanceUI;

	bool isHidden;

	void Start() {
		inventoryUI = GameObject.FindGameObjectWithTag ("InventoryUI");
		shopUI = GameObject.FindGameObjectWithTag ("ShopUI");
		balanceUI = GameObject.FindGameObjectWithTag ("Balance").GetComponent<Text> ();

		playerCtrl = this.gameObject;

		InitInventory ();
		Show (false);

		balanceUI.text = charBalance.ToString();

		shopItemList = new List<Item> ();
		shopItemList.Add (new Item (Item.ItemName.Shovel));
		shopItemList.Add (new Item (Item.ItemName.Axe));
		for(int i = 2;i<16;i++)
			shopItemList.Add (new Item ());
		

	}

	void Update() {

		if (Input.GetKeyDown (KeyCode.I)) {
			if (isHidden) {
				Show (false); // Hide all elements first
				Show (true, uiElem.Inventory);
			} else {
				Show (false);
			}
		} else if (Input.GetKeyDown (KeyCode.O)) {
			if (isHidden) {
				Show (false);
				Show (true, uiElem.Shop);
			} else {
				Show (false);
			}
		}
	}
		
	public void AddItem(Item.ItemName name, int q=1) {
		bool ok = false;
		int i;
		for (i = 0; i < slotsCount; i++) {
			if(itemList[i].Name == name) {
				//This slot is contains the item. Stack the item here
				quantity[i] += q;
				ok = true;
				break;
			}
		}

		//The item doesn't exist in inventory. Find the first free slot and place it there.
		if (!ok) {
			for (i = 0; i < slotsCount; i++) {
				if (quantity [i] == 0) {
					//This slot is free. Add the item here
					itemList[i] = new Item(name);
					quantity[i] = q;
					ok = true;
					break;
				}
			}
		}

		itemSlots [i].GetComponent<InvCtrl> ().UpdateSlot ();
		shopItemSlots[i].GetComponent<ShopSlots> ().UpdateSlot ();
	}

	public void BuyItem(Item item, int quantity=1) {
		if (item.ItemPrice <= charBalance) {
			charBalance -= item.ItemPrice;
			AddItem (item.Name, quantity);
		}
	}

	public void SellItem() {
		
	}

	public void Show(bool val, uiElem elem = uiElem.All) {
		if (val) {
			playerCtrl.GetComponent<FirstPersonController> ().FreezeMovement = true; // Prevent the camera from following the mouse direction
			playerCtrl.GetComponent<FirstPersonController> ().SetLockCursor (false); // Unlock the cursor and make it visible so it can be used

			if (elem == uiElem.Inventory) {
				inventoryUI.GetComponent<CanvasGroup> ().alpha = 1f; 
				inventoryUI.GetComponent<CanvasGroup> ().blocksRaycasts = true; 
				inventoryUI.GetComponent<CanvasGroup> ().interactable = true; 
			} else if (elem == uiElem.Shop) {
				shopUI.GetComponent<CanvasGroup> ().alpha = 1f; 
				shopUI.GetComponent<CanvasGroup> ().blocksRaycasts = true; 
				shopUI.GetComponent<CanvasGroup> ().interactable = true; 
			} else if (elem == uiElem.All) {
				inventoryUI.GetComponent<CanvasGroup> ().alpha = 1f; 
				inventoryUI.GetComponent<CanvasGroup> ().blocksRaycasts = true; 
				inventoryUI.GetComponent<CanvasGroup> ().interactable = true; 
				shopUI.GetComponent<CanvasGroup> ().alpha = 1f; 
				shopUI.GetComponent<CanvasGroup> ().blocksRaycasts = true; 
				shopUI.GetComponent<CanvasGroup> ().interactable = true; 
			}

			isHidden = false;
		} else {
			playerCtrl.GetComponent<FirstPersonController> ().FreezeMovement = false;
			playerCtrl.GetComponent<FirstPersonController> ().SetLockCursor (true);

			if (elem == uiElem.Inventory) {
				inventoryUI.GetComponent<CanvasGroup> ().alpha = 0f; 
				inventoryUI.GetComponent<CanvasGroup> ().blocksRaycasts = false; 
				inventoryUI.GetComponent<CanvasGroup> ().interactable = false; 
			} else if (elem == uiElem.Shop) {
				shopUI.GetComponent<CanvasGroup> ().alpha = 0f; 
				shopUI.GetComponent<CanvasGroup> ().blocksRaycasts = false; 
				shopUI.GetComponent<CanvasGroup> ().interactable = false; 
			} else if (elem == uiElem.All) {
				inventoryUI.GetComponent<CanvasGroup> ().alpha = 0f; 
				inventoryUI.GetComponent<CanvasGroup> ().blocksRaycasts = false; 
				inventoryUI.GetComponent<CanvasGroup> ().interactable = false; 
				shopUI.GetComponent<CanvasGroup> ().alpha = 0f; 
				shopUI.GetComponent<CanvasGroup> ().blocksRaycasts = false; 
				shopUI.GetComponent<CanvasGroup> ().interactable = false; 
			}

			isHidden = true;
		}
	}

	public bool IsHidden {
		get { return this.isHidden; }
	}

	void InitInventory() {
		itemSlots = new List<GameObject> ();
		shopItemSlots = new List<GameObject> ();

		itemList = new List<Item> ();
		quantity = new List<int> ();

		for (int i = 0; i < slotsCount; i++) {
			itemList.Add (new Item ());
			quantity.Add (0);

			itemSlots.Add(Instantiate (Resources.Load<GameObject> ("Prefabs/ItemSlot")));
			itemSlots[i].GetComponent<InvCtrl> ().slotId = i;
			itemSlots[i].transform.SetParent(inventoryUI.transform,false);

			shopItemSlots.Add(Instantiate (Resources.Load<GameObject> ("Prefabs/ShopItemSlot")));
			shopItemSlots[i].GetComponent<ShopSlots> ().slotId = i;
			shopItemSlots [i].GetComponent<ShopSlots> ().shopSlot = false;

			shopItemSlots[i].transform.SetParent(shopUI.transform.GetChild(0),false);
		}

		for (int i = slotsCount; i < slotsCount+16; i++) {
			shopItemSlots.Add(Instantiate (Resources.Load<GameObject> ("Prefabs/ShopItemSlot")));
			shopItemSlots[i].GetComponent<ShopSlots> ().slotId = i;
			shopItemSlots [i].GetComponent<ShopSlots> ().shopSlot = true;
			shopItemSlots[i].transform.SetParent(shopUI.transform.GetChild(1),false);
		}
	}

	public List<Item> ItemList {
		get { return this.itemList; }
		set { this.itemList = value; }
	}

	public List<int> Quantity {
		get { return this.quantity; }
		set { this.quantity = value; }
	}

	public List<Item> ShopItemList {
		get { return this.shopItemList; }
		set { this.shopItemList = value; }
	}
		


}
