﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopSlots : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {

	Item item;
	int quantity;
	public int slotId = 0;
	public bool shopSlot;

	GameObject invTooltip;
	CharInventory charInventory;
	Text bsButton;

	bool displayName = false;

	void Start () {
		invTooltip = GameObject.FindGameObjectWithTag ("InvItemTooltip");	
		charInventory = GameObject.FindGameObjectWithTag ("Player").GetComponent<CharInventory>();
		bsButton = GameObject.FindGameObjectWithTag ("BSButton").GetComponentInChildren<Text> ();

		UpdateSlot ();
	}

	void Update() {
		if (displayName) {
			invTooltip.transform.GetChild (1).GetComponent<Text> ().text = item.ItemTitle;
			invTooltip.transform.position = Input.mousePosition;
			Debug.Log ("show");
		}
	}

	public void OnPointerEnter(PointerEventData eventData) {
		if(item.ItemTitle != null) {
			displayName = true;
			invTooltip.GetComponent<CanvasGroup> ().alpha = 1f;
		}
	}

	public void OnPointerExit(PointerEventData eventData) {
		displayName = false;
		invTooltip.GetComponent<CanvasGroup> ().alpha = 0f;
	}

	public void OnPointerDown( PointerEventData eventData )
	{
	}

	public void OnPointerUp( PointerEventData eventData )
	{
	}

	public void OnPointerClick( PointerEventData eventData )
	{
		if (shopSlot) {
			bsButton.text = "Buy [" + item.ItemPrice.ToString () + " G]";
		} else {
			bsButton.text = "Sell ["+ (item.ItemPrice * 0.9f).ToString() +" G]";
		}
	
		GameObject.FindGameObjectWithTag ("ItemText").GetComponent<Text> ().text = item.ItemTitle;

		Debug.Log( "Clicked! " + gameObject.name );
	}
	

	public void UpdateSlot() {
		if (slotId >= charInventory.slotsCount) {
			item = charInventory.ShopItemList [slotId-charInventory.slotsCount];
			quantity = 0;
		} else {
			item = charInventory.ItemList [slotId];
			quantity = charInventory.Quantity [slotId];
		}

		transform.GetChild(0).GetComponent<Image> ().sprite = item.ItemImage;
		transform.GetChild(0).GetComponent<Image> ().color = item.ItemImage!=null?new Color(1,1,1,1):new Color(0,0,0,0);
		transform.GetChild(1).GetComponent<Text> ().text = quantity==0?"":quantity.ToString();
	}

}
