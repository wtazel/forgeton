﻿using UnityEngine;
using System.Collections;

public class CombineM {


	static public void combineNeMeshes(GameObject obj, string prefabName, Vector3[] positions, bool changeTag = true) {
		Vector3 position = obj.transform.position;
		obj.transform.position = Vector3.zero;

		GameObject aux = GameObject.Instantiate (Resources.Load<GameObject> ("Prefabs/" + prefabName));
		int meshCount = positions.Length;

		MeshFilter auxMeshFilter = aux.GetComponent<MeshFilter> ();

		CombineInstance[] combine = new CombineInstance[meshCount];
		int i = 0;

		while (i < meshCount) {
			combine [i].mesh = auxMeshFilter.sharedMesh;
			Transform auxTransfrom = new GameObject ().transform;

			auxTransfrom.position = positions[i];
			auxTransfrom.localScale = new Vector3 (1, 1, 1);
			auxTransfrom.rotation = Quaternion.identity;

			combine [i].transform = auxTransfrom.localToWorldMatrix;
			GameObject.Destroy (auxTransfrom.gameObject);
			i++;
		}

		obj.transform.GetComponent<MeshFilter> ().mesh = new Mesh ();
		obj.transform.GetComponent<MeshFilter> ().mesh.CombineMeshes (combine, true, true);
		obj.transform.GetComponent<Renderer> ().material = aux.GetComponent<Renderer> ().material;
		obj.transform.gameObject.SetActive(true);

		obj.transform.position = position;

		obj.AddComponent<MeshCollider> ();  

		GameObject.Destroy (aux);

		if(changeTag)
			obj.tag = "Combined";
	}

	static public void combineMeshes(GameObject obj, bool changeTag = true)
	{
		if (obj.transform.GetChild (0) == null)
			return;
		
		//Zero transformation is needed because of localToWorldMatrix transform
		Vector3 position = obj.transform.position;
		obj.transform.position = Vector3.zero;

		//whatever man
		MeshFilter[] meshFilters = obj.GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combine = new CombineInstance[meshFilters.Length];
		int i = 0;
		while (i < meshFilters.Length) {
			combine[i].mesh = meshFilters[i].sharedMesh;
			combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
			meshFilters[i].gameObject.SetActive(false);
			i++;
		}
		obj.transform.GetComponent<MeshFilter>().mesh = new Mesh();
		obj.transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine, true, true);
		obj.transform.GetComponent<Renderer> ().material = obj.transform.GetChild (0).GetComponent<Renderer> ().material;
															//Resources.Load<Material> ("Materials/Grass");
		obj.transform.gameObject.SetActive(true);

		//Reset position
		obj.transform.position = position;

		//Adds collider to mesh
		if(obj.GetComponent<MeshCollider>() == null)
			obj.AddComponent<MeshCollider> ();  

		if(changeTag)
			obj.tag = "Combined";
	}

	static public void splitMeshes(GameObject obj) {
		int i = 0;

		while (i < obj.transform.childCount) {
			obj.transform.GetChild (i).gameObject.SetActive (true);
			//Some objects such as trees are composed of blocks. Access their children if they have.
			if (obj.transform.GetChild (i).childCount > 0) {
				obj.transform.GetChild (i).gameObject.GetComponentInChildren<MeshRenderer> ().enabled = true;
			} else {
				obj.transform.GetChild (i).GetComponent<MeshRenderer> ().enabled = true;
			}
			i++;
		}

		obj.transform.GetComponent<MeshFilter> ().mesh = null;
		obj.transform.GetComponent<Renderer> ().material = null;
		GameObject.Destroy(obj.transform.GetComponent<MeshCollider> ());

		obj.tag = "Untagged";
	}

	static public bool isSplit(GameObject obj) {
		return !(obj.transform.GetChild (0).gameObject.activeSelf);
	}


	static public void CombineX(GameObject parentObj)
	{

		Vector3 origPos = parentObj.transform.position;
		parentObj.transform.position = Vector3.zero;

		GameObject[] Objects = new GameObject[parentObj.transform.childCount];

		for (int i = 0; i < parentObj.transform.childCount; i++) {
			Objects [i] = parentObj.transform.GetChild (i).gameObject;
		}

		// Find all mesh filter submeshes and separate them by their cooresponding materials
		ArrayList materials = new ArrayList();
		ArrayList combineInstanceArrays = new ArrayList();

		foreach( GameObject obj in Objects ) 
		{
			if(!obj)
				continue;

			MeshFilter[] meshFilters = obj.GetComponentsInChildren<MeshFilter>();

			foreach( MeshFilter meshFilter in meshFilters )
			{
				MeshRenderer meshRenderer = meshFilter.GetComponent<MeshRenderer>();

				// Handle bad input
				if(!meshRenderer) { 
					Debug.LogError("MeshFilter does not have a coresponding MeshRenderer."); 
					continue; 
				}
				if(meshRenderer.materials.Length != meshFilter.sharedMesh.subMeshCount) { 
					Debug.LogError("Mismatch between material count and submesh count. Is this the correct MeshRenderer?"); 
					continue; 
				}

				for (int s = 0; s < meshFilter.sharedMesh.subMeshCount; s++) {
					int materialArrayIndex = Contains (materials, meshRenderer.sharedMaterials [s].name);
					if (materialArrayIndex == -1) {
						materials.Add (meshRenderer.sharedMaterials [s]);
						materialArrayIndex = materials.Count - 1;
					} 
					combineInstanceArrays.Add (new ArrayList ());

					CombineInstance combineInstance = new CombineInstance ();
					combineInstance.transform = meshRenderer.transform.localToWorldMatrix;
					combineInstance.subMeshIndex = s;
					combineInstance.mesh = meshFilter.sharedMesh;
					(combineInstanceArrays [materialArrayIndex] as ArrayList).Add (combineInstance);
				}
			}
		}

		// For MeshFilter
		{
			// Get / Create mesh filter
			MeshFilter meshFilterCombine = parentObj.GetComponent<MeshFilter>();
			if(!meshFilterCombine)
				meshFilterCombine = parentObj.AddComponent<MeshFilter>();

			// Combine by material index into per-material meshes
			// also, Create CombineInstance array for next step
			Mesh[] meshes = new Mesh[materials.Count];
			CombineInstance[] combineInstances = new CombineInstance[materials.Count];

			for( int m = 0; m < materials.Count; m++ )
			{
				CombineInstance[] combineInstanceArray = (combineInstanceArrays[m] as ArrayList).ToArray(typeof(CombineInstance)) as CombineInstance[];
				meshes[m] = new Mesh();
				meshes[m].CombineMeshes( combineInstanceArray, true, true );

				combineInstances[m] = new CombineInstance();
				combineInstances[m].mesh = meshes[m];
				combineInstances[m].subMeshIndex = 0;
			}

			// Combine into one
			meshFilterCombine.sharedMesh = new Mesh();
			meshFilterCombine.sharedMesh.CombineMeshes( combineInstances, false, false );

			// Destroy other meshes
			foreach( Mesh mesh in meshes )
			{
				mesh.Clear();
				//GameObject.Destroy(mesh);
			}

			if(parentObj.GetComponent<MeshCollider>() == null)
				parentObj.AddComponent<MeshCollider> ();  
			
			parentObj.transform.position = origPos;

			foreach (GameObject gm in Objects) {
				gm.SetActive (false);
			}
		}

		// For MeshRenderer
		{
			// Get / Create mesh renderer
			MeshRenderer meshRendererCombine = parentObj.GetComponent<MeshRenderer>();
			if(!meshRendererCombine)
				meshRendererCombine = parentObj.AddComponent<MeshRenderer>();    

			// Assign materials
			Material[] materialsArray = materials.ToArray(typeof(Material)) as Material[];
			meshRendererCombine.materials = materialsArray;    
		}
	}

	static private int Contains (ArrayList searchList, string searchName)
	{
		for (int i = 0; i < searchList.Count; i++) {
			if (((Material)searchList [i]).name == searchName) {
				return i;
			}
		}
		return -1;
	}


}