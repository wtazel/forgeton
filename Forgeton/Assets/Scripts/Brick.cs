﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brick : MonoBehaviour {

	int support = 100;

	Text supportUItxt;
	Image supportUIimg;

	MouseTarget mt;
	CharInventory ci;

	public int blockTypeId;

	void Start() {
		supportUItxt = GameObject.FindGameObjectWithTag ("SupportUI").GetComponent<Text> ();
		supportUIimg = GameObject.FindGameObjectWithTag ("SupportUI").transform.GetChild(0).GetComponent<Image> ();

		mt = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<MouseTarget> ();
		ci = GameObject.FindGameObjectWithTag ("Player").GetComponent<CharInventory> ();

	}

	void OnMouseOver(){
		if (Input.GetMouseButtonUp(0) && ci.IsHidden) {
			supportUIimg.sprite = Sprite.Create (gameObject.GetComponent<Renderer> ().material.mainTexture as Texture2D, new Rect (0, 0, 128, 128), new Vector2 (0.5f, 0.5f));
			supportUIimg.color = new Color (1f, 1f, 1f, 1f);

			if (blockTypeId == BlockDefines.block_wood) {
				if (mt.CurrentTool == 3) {
					support -= Random.Range (5, 9);
				}
			} else if (blockTypeId == BlockDefines.block_leaves) {
				support -= 100;
			} else {
				if (mt.CurrentTool == 2) {
					support -= Random.Range (25, 65);
				} else if (mt.CurrentTool == 1) {
					support -= Random.Range (1, 3);
				}
			}

			supportUItxt.text = support.ToString () + " / 100";
			if (support <= 0) {
				if (blockTypeId == BlockDefines.block_dirt || blockTypeId == BlockDefines.block_deepdirt) {
					ci.AddItem (Item.ItemName.Dirt, Random.Range (1, 5));
				} else if (blockTypeId == BlockDefines.block_grass && Random.Range (0, 6) == 0) {
					ci.AddItem (Item.ItemName.GrassSeeds);
				} else if (blockTypeId == BlockDefines.block_wood) {
					ci.AddItem (Item.ItemName.Wood);
				}

				supportUItxt.text = "";
				supportUIimg.color = new Color (0f, 0f, 0f, 0f);
				Destroy (this.gameObject);
			}
			
		}
	}

	void OnBecameInvisible() {
		enabled = false;
	}

	void OnBecameVisible() {
		enabled = true;

	}
}
