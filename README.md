# README #

Forgeton is a sandbox building blocks game similar to Minecraft. The goal is to give the player the posibility of building much more detailed creations, while 
retaining the "simplicity" feeling as much as possible.


# How much of a Minecraft clone is this ?

Forgeton will appear like a cheap Minecraft clone, but it will soon feel like a different and enjoyable game. Even if the mechanics, the blocks and other aspects of Forgeton
are similar to those in Minecraft, new posibilities of gameplay will quickly be revealed along with the potential of fun.

It is worth mentioning that Forgeton has also its own unique aspects and content. The differences in the game mechanics, challenges, events and ultimately the posibility 
of customization are just few points that will erase the "Minecraft" feeling. Added to the list are new blocks, new objects, new worlds and new stuff to explore.


# Technical details about Forgeton

The game is being built in the Unity Engine. It's available for PC only on Microsoft Windows, although the game will be ported on other operating systems and on other platforms
at some point. 

Forgeton is currently in the Pre-Alpha state. It can be tested without installation.

Minimum system requirements are:
	Intel Core i3 or equivalent
	4.00GB of RAM
	Nvidia GeForce 920m or equivalent
	Microsoft Windows
	
These system requirements might suffer changes throughout the developement proccess.


# Will Forgeton be free ?

It has not been established yet if the game will be freeware or if it'll require purchasing.